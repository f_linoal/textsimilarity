/**
 * Created by linoal on 14/05/08.
 */
import scala.collection.mutable
object BigramVector {
  def cosineDiff(bigramA:mutable.Set[(String,String)], bigramB:mutable.Set[(String,String)]):Double = {
    val innerProduct = countMatch(bigramA,bigramB).asInstanceOf[Double]
    val absA = math.sqrt(bigramA.size.asInstanceOf[Double])
    val absB = math.sqrt(bigramB.size.asInstanceOf[Double])
    println("innerProduct(match)=" + innerProduct + ", absA=" + absA + ", absB=" + absB)
    innerProduct / (absA * absB)
  }

  def countMatch(bigramA:mutable.Set[(String,String)], bigramB:mutable.Set[(String,String)]):Int = {
    var matchCount = 0
    bigramA.foreach({ tupleA =>
      bigramB.foreach({ tupleB =>
        if(tupleA==tupleB){
          println("match " + tupleA)
          matchCount = matchCount + 1
        }
      })
    })
    matchCount
  }
}
