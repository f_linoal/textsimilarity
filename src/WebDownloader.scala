/**
 * Created by linoal on 14/05/07.
 */
import scala.collection.mutable

import java.net._
import java.io._

object WebDownloader {
  def downloadAndMake2GramList(articleName:String):mutable.Set[(String,String)] = {
    wordListTo2Gram(downloadWordList(articleName))
  }

  def downloadWordList(articleName:String):Array[String] = {
    httpToWordList(downloadHTTP(articleName))
  }

  def downloadHTTP(articleName:String): String = {
    var content: String = ""
    try {
      val url = new URL("http://en.wikipedia.org/wiki/" + articleName)
      val connection = url.openConnection().asInstanceOf[HttpURLConnection]
      connection.setDoInput(true)
      val inStream = connection.getInputStream()
      val input = new BufferedReader(new InputStreamReader(inStream, "UTF-8"))
      var line = ""
      while ( {
        line = input.readLine()
        line ne null
      }) {
        content = content.concat(line + "\n")
      }
      input.close()
      connection.disconnect()
    } catch {
      case e: Exception => println(e)
    } finally {
    }
    content
  }

  def httpToWordList(http:String):Array[String] = {
    val nl:String = getNewLineEscape
    http.
      replaceAll("<.+?>", "").// remove tags
      replaceAll("[^a-zA-Z0-9\\s]", "").// just consider alphabets & numbers
      replaceAll("(\\t|\\s\\s)+", " ").// remove duplicate spaces
      split("\\s")
  }

  def wordListTo2Gram(wordList:Array[String]):mutable.Set[(String,String)] = {
    val twoGramList = mutable.Set[(String,String)](("",""))
    for(i:Int <- 1 to wordList.size-1){
      twoGramList += Tuple2(wordList(i-1), wordList(i))
    }
    println("Generated 2gram list.")
    twoGramList
  }


  def getNewLineEscape:String = {
    System.getProperty("line.separator") // new line
  }
}
