/**
 * Created by linoal on 14/05/07.
 */
import scala.collection.mutable
object TextSimilarity {
  def main(args: Array[String]) {
    println("-- Text Similarity --")
    //calc("World_War_1", "World_War_2") // 0.13644339296513402
    calc("Hideki_Matsui", "Partial_differential_equation") // 0.08561993368968131
    //calc("Space_Dandy", "Pocket_Monster") // 0.10488448753117183
    //calc("Peppermint","Dust_explosion") // 0.144055
    //calc("Encyclopedia", "Wikipedia") // 0.09907765756752826
    //calc("Steve_jobs", "Ipad") // 0.1135
    //calc("Kite","Euglena")// 0.09616
  }

  def calc(articleA:String, articleB:String) = {
    println( BigramVector.cosineDiff(make2Grams(articleA), make2Grams(articleB)) )
  }

  def make2Grams(articleName:String):mutable.Set[(String,String)] = {
    WebDownloader.downloadAndMake2GramList(articleName)
  }
}
